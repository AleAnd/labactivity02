package hellopackage;
import secondpackage.Utilities;
import java.util.Scanner;
import java.util.Random;

public class Greeter{
    public static void main(String[]args){
        Scanner scanner = new Scanner(System.in);
        Utilities util = new Utilities();

        System.out.println("Input a number");
        int userInput = scanner.nextInt();
        int doubled = util.doubleMe(userInput);
        System.out.println(doubled);
    }
}